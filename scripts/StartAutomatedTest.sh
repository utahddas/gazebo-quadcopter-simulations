killgazebo

WORLD_FILE=$1 
NUM_QUADS=$2
LOCATION=$3
MISSION_FILE=$4

cd /home/taylorwelker

source /usr/share/gazebo/setup.sh
#GAZEBO_RESOURCE_PATH="$GAZEBO_RESOURCE_PATH:/usr/share/gazebo-9/worlds" gazebo --verbose /usr/share/gazebo-9/worlds/$WORLD_FILE &
GAZEBO_RESOURCE_PATH="$GAZEBO_RESOURCE_PATH:/home/taylorwelker" gazebo --verbose $WORLD_FILE &

sleep 10

counter=0
max_count=$NUM_QUADS

#echo $WORLD_FILE >> /home/taylorwelker/intellij_bug.txt
#echo $NUM_QUADS >> /home/taylorwelker/intellij_bug.txt
#echo $LOCATION >> /home/taylorwelker/intellij_bug.txt
#echo $MISSION_FILE >> /home/taylorwelker/intellij_bug.txt

#xterm -e python ~/ardupilot/Tools/autotest/sim_vehicle.py -v ArduCopter -f gazebo-iris --console -L $LOCATION -m --mav10 -I$counter &
#xterm -e mavproxy.py --master=udp:127.0.0.1:14550 --out=udpout:192.168.0.100:14550 --out=udpout:127.0.0.1:13552 --out=udpout:127.0.0.1:14553 &
#xterm -e python ~/breccia_quadrotor/dronekit-python/examples/SimMission/SimMission.py --connect 127.0.0.1:14552 --mission $MISSION_FILE &

while [ $counter -le $max_count ]
do
        xterm -e python ~/ardupilot/Tools/autotest/sim_vehicle.py -v ArduCopter -f gazebo-iris --console -L $LOCATION -m --mav10 -I$counter &
	((counter++))
done

sleep 15

counter=0
while [ $counter -le $max_count ]
do
        xterm -e mavproxy.py --master=udp:127.0.0.1:14$(($counter + 55))0 --out=udpout:192.168.0.100:14$(($counter + 55))0 --out=udpout:127.0.0.1:14$(($counter + 55))2 --out=udpout:127.0.0.1:14$(($counter + 55))3 &
	((counter++))
done

sleep 15

counter=0
while [ $counter -le $max_count ]
do
        xterm -e python ~/breccia_quadrotor/dronekit-python/examples/SimMission/SimMission.py --connect 127.0.0.1:14$(($counter + 55))2 --mission $MISSION_FILE &
	((counter++))
done
