killgazebo

source /usr/share/gazebo/setup.sh
#GAZEBO_RESOURCE_PATH="$GAZEBO_RESOURCE_PATH:/usr/share/gazebo-7/worlds" gazebo --verbose /usr/share/gazebo-7/worlds/uofu_web.world &
GAZEBO_RESOURCE_PATH="$GAZEBO_RESOURCE_PATH:/usr/share/gazebo-9/worlds" gazebo --verbose /usr/share/gazebo-9/worlds/my_mesh_actor.world &

sleep 10

counter=0
max_count=0

while [ $counter -le $max_count ]
do
	xterm -e python ~/Documents/bigdrive_link/ardupilot/Tools/autotest/sim_vehicle.py -v ArduCopter -L UofU2 -f gazebo-iris -m --mav10 --mavlink-gimbal -I$counter &
	((counter++))
done

sleep 30

counter=0
while [ $counter -le $max_count ]
do
	xterm -e mavproxy.py --master=udp:127.0.0.1:14$(($counter + 55))0 --out=udpout:192.168.0.100:14$(($counter + 55))0 --out=udpout:127.0.0.1:14$(($counter + 55))2 --out=udpout:127.0.0.1:14$(($counter + 55))3 &
	((counter++))
done

sleep 30

counter=0
while [ $counter -le $max_count ]
do
	xterm -e python ~/breccia_quadrotor/dronekit-python/examples/UserControlRough/UserControlRough.py --connect 127.0.0.1:14$(($counter + 55))2 &
	((counter++))
done

