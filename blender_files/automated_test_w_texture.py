import bpy
import os

# Clear the env
bpy.ops.object.delete(use_global=False)

# Import the .tif file as a 3D mesh and reposition
bpy.ops.importgis.georaster(filepath="/home/taylorwelker/blender_files/output_merged.tif",importMode="DEM")
bpy.ops.object.convert(target='MESH')
bpy.context.object.location[2] = -1450

# Add texture to the mesh
ob = bpy.data.objects[0]

# bpy.ops.image.open(filepath="C:\\Users\\taylo\\Documents\\uofu_texture_labeless_off40.PNG", directory="C:\\Users\\taylo\\Documents\\", files=[{"name":"uofu_texture_labeless_off40.PNG", "name":"uofu_texture_labeless_off40.PNG"}], show_multiview=False)
tex = bpy.data.textures.new("SomeName", 'IMAGE')
tex.image = bpy.data.images.load("/home/taylorwelker/blender_files/uofu_texture_labeless_off40.PNG")
tex.use_alpha = True

mat = bpy.data.materials.new('TexMat')
mat.use_shadeless = True
mtex = mat.texture_slots.add()
mtex.texture = tex
mtex.texture_coords = 'UV'
mtex.use_map_color_diffuse = True

#ob.data.materials.append(mat)
ob.active_material = mat

# Export the mesh
# Get the appropriate file paths
target_path_stl="/home/taylorwelker/blender_files/auto.stl"
target_path_dae="/home/taylorwelker/blender_files/auto.dae"

# Export to .stl and .dae files
bpy.ops.export_mesh.stl(filepath=target_path_stl)
bpy.ops.wm.collada_export(filepath=target_path_dae)
