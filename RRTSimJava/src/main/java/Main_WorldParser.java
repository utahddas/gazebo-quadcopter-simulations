import gazebo_world_gen.GazWorldGen;
import sim.Simulation;

import java.io.FileWriter;

public class Main_WorldParser {
    public static void main(String [] args){

        GazWorldGen gazWorldGen = new GazWorldGen(args[0]+".stl",args[0]+".dae", Integer.parseInt(args[2]));

        try{
            FileWriter fileWriter = new FileWriter(args[1]);
            fileWriter.write(gazWorldGen.buildWorld());
            fileWriter.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}

