import java.io.*;
import java.net.*;

class DronekitConnTest {
    public static void main(String args[]) throws Exception {
        String fromClient;
        String toClient;

        ServerSocket server = new ServerSocket(8080);
        System.out.println("wait for connection on port 8080");

        boolean run = true;

        Socket client = server.accept();
        System.out.println("got connection on port 8080");
        BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        PrintWriter out = new PrintWriter(client.getOutputStream(),true);

        int i = 0;

        while(run) {

            i=i+1;
            fromClient = in.readLine();
            System.out.println("received: " + fromClient);

            if(fromClient.equals("Bye"))
                run = false;
            else {
                if(i%1000000 == 0){
                    System.out.println("send update: ");
                    out.println("remove.txt");
                }
                else{
                    System.out.println("send Everything is A-OK");
                    out.println("Everything is A-OK");
                }


            }
        }
        System.exit(0);
    }
}
