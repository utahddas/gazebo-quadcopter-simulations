import gazebo_world_gen.GazWorldGen;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main_Automation {
    public static void main(String [] args){

        /*
            1. Execute bash to run Blender Python Script (and move files to /usr/share/gazebo-9/worlds)
            2. Use GazWorldGen to create the .world  file
            3. Move the .world file to /usr/share/gazebo-9/worlds
            4. Execute final bash script to run the simulation

            *Steps to be added:
            + Automatically get the tif and png files
            + Somehow modify the wind plugin/world file
            + Somehow align png with tif using MATLAB script

         */



	    // SO, IF THE QUAD LIKES NED CONFIGURATION AND GAZEBO LIKES ENU CONFIGURATION, THEN IT IS POSSIBLE THAT THE TRANSFORMATION WOULD BE (FROM GAZEBO TO QUAD PERSEPCTIVE X->Y AND Y->-X...

        try {

            String tif_file = "output_merged.tif";
            String png_file = "uofu_texture_labeless_off40.PNG";
            String stl_file = "counting_stars.stl";
            String dae_file = "counting_stars.dae";

            String python_arguments = tif_file + " " + png_file + " " + stl_file + " " + dae_file;

            String[] cmd ={"/bin/bash", "/home/taylorwelker/TestBash.sh"};


            Runtime rt = Runtime.getRuntime();
            final Process pr = rt.exec("/home/taylorwelker/BlenderMeshGen.sh " + python_arguments);

            // Create variables to hold the output data
            StringBuilder octave_output = new StringBuilder();
            StringBuilder error_output = new StringBuilder();


            new Thread(new Runnable() {
                @Override
                public void run(){
                    // Set the readers to the input streams
                    BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                    BufferedReader error = new BufferedReader(new InputStreamReader(pr.getErrorStream()));

                    // This holds the individual lines
                    String line = null;

                    // Grab the output and store it in the appropriate StringBuilder
                    try{
                        while((line = input.readLine()) != null)
                            octave_output.append(line);
                        while((line = error.readLine()) != null)
                            error_output.append(line);
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }).start();
            pr.waitFor();

            if(octave_output.length() > 0)
                System.out.println(octave_output.toString());
            else
                System.out.println(error_output.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

        catch (InterruptedException e) {
            e.printStackTrace();
        }

        GazWorldGen gazWorldGen = new GazWorldGen(args[0]+".stl",args[0]+".dae", Integer.parseInt(args[2]));

        try{
            FileWriter fileWriter = new FileWriter(args[1]);
            fileWriter.write(gazWorldGen.buildWorld());
            fileWriter.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        // Move the .world file
        try {

            String world_file = args[1];
            int num_quads = Integer.valueOf(args[2]);
            String location = "UofU4";
            String mission_file = "test_mission3.txt";

            String simulation_arguments = world_file + " " + num_quads + " " + location + " " + mission_file;
            String cmds[] = {"bash", "-c", "/home/taylorwelker/StartAutomatedTest.sh " + simulation_arguments};
            Runtime rt = Runtime.getRuntime();
            final Process pr = rt.exec(cmds);

            // Create variables to hold the output data
            StringBuilder octave_output = new StringBuilder();
            StringBuilder error_output = new StringBuilder();


            new Thread(new Runnable() {
                @Override
                public void run(){
                    // Set the readers to the input streams
                    BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
                    BufferedReader error = new BufferedReader(new InputStreamReader(pr.getErrorStream()));

                    // This holds the individual lines
                    String line = null;

                    // Grab the output and store it in the appropriate StringBuilder
                    try{
                        while((line = input.readLine()) != null)
                            octave_output.append(line);
                        while((line = error.readLine()) != null)
                            error_output.append(line);
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }).start();
            pr.waitFor();

            if(octave_output.length() > 0)
                System.out.println(octave_output.toString());
            else
                System.out.println(error_output.toString());

            //Process procBuildScript = new ProcessBuilder("/home/taylorwelker/BlenderMeshGen.sh", python_arguments).start();

        } catch (IOException e) {
            e.printStackTrace();
        }

        catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
