package state;

import javafx.geometry.Point3D;

/*
 state.AreaBound.java
 Author: Taylor Welker
 June 4, 2018

 The purpose of this class is to hold info regarding the wind behaviour within a 3D section of the environment.

 This class defines a 3D boundary using an XYZ coordinate system (X = Lon, Y = Lat, Z = alt).  Then, using this boundary,
 it stores the wind behaviour within this area as a 3D vector.  It contains an internal function to ascertain whether or
 not a particular GPS location falls within its boundary.
*/
public class AreaBound {

    // Class variables
    private double min_lon;
    private double max_lon;
    private double min_lat;
    private double max_lat;
    private double min_alt;
    private double max_alt;
    private Point3D wind_pattern;

    // All this information should come from the JSON input files
    public AreaBound(double new_min_lon, double new_max_lon, double new_min_lat, double new_max_lat, double new_min_alt,
                     double new_max_alt, Point3D new_wind_pattern){
        min_lon = new_min_lon;
        max_lon = new_max_lon;
        min_lat = new_min_lat;
        max_lat = new_max_lat;
        min_alt = new_min_alt;
        max_alt = new_max_alt;
        wind_pattern = new_wind_pattern;
    }


    // Function to check to see if a position is in bounds
    public boolean isInBounds(Point3D pos){

        boolean result = true;      // Assume true till proven false

        // Get the lon, lat and alt from the Point3D object and store in local variables
        double lon = pos.getX();
        double lat = pos.getY();
        double alt = pos.getZ();

        // If our lon, lat, or alt is less than their corresponding minimum or greater than or equal to the
        // corresponding maximum, then we will return false (we are out of the bounds)
        if((lon < min_lon) || (lon >= max_lon)){result = false;}
        else if((lat < min_lat) || (lat >= max_lat)){result = false;}
        else if((alt < min_alt) || (alt >= max_alt)){result = false;}

        return result;
    }

    @Override
    public String toString() {
        return "state.AreaBound{" +
                "min_lon : " + min_lon +
                ", max_lon : " + max_lon +
                ", min_lat : " + min_lat +
                ", max_lat : " + max_lat +
                ", min_alt : " + min_alt +
                ", max_alt : " + max_alt +
                ", Point3D : {" +
                "x : " + wind_pattern.getX() +
                ", y : " + wind_pattern.getY() +
                ", z : " + wind_pattern.getZ() +
                "}}";
    }

    // Getters
    public Point3D getWind_pattern() {
        return wind_pattern;
    }

    public double getMin_lon() {
        return min_lon;
    }

    public double getMax_lon() {
        return max_lon;
    }

    public double getMin_lat() {
        return min_lat;
    }

    public double getMax_lat() {
        return max_lat;
    }

    public double getMin_alt() {
        return min_alt;
    }

    public double getMax_alt() {
        return max_alt;
    }
}
