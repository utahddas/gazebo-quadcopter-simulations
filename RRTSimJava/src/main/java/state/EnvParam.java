package state;

import javafx.geometry.Point3D;
import state.AreaBound;

import java.util.ArrayList;
import java.util.Random;

/*
 state.EnvParam.java
 Author: Taylor Welker
 June 4, 2018

 The purpose of this class is to hold info that relates to the simulation's environment.  In this case, just the wind.

 This class was designed to store information specific to the wind patterns within the environment, but could be expanded
 to include other environment dependant variables.  Currently the class holds the wind velocity vector (3D) that is being
 exerted on the quadcopter in the current timestep, as well as a list of "state.AreaBound" object that are used to describe the
 dominant wind behavior an any given point in the environment.  This wind vector is updated based on the new position
 obtained from the previous timestep using the "update_wind" method.

 There is some additional functionality still in Beta (and definitely not working yet) that is meant to add slight
 fluctuations to the wind behavior based on random chance.  The method associated with that behavior is called
 "rand_wind".
*/
public class EnvParam {

    // Class Variables
    Random rand;                        // Used to add randomness to the wind (if desired)
    Point3D wind;                       // Holds current wind velocity vector
    ArrayList<AreaBound> wind_sectors;  // Holds description of wind at every area within the environment

    public EnvParam(ArrayList<AreaBound> new_wind_sectors){
        rand = new Random();                // Get class to create random numbers
        wind = new Point3D(0,0,0);  // This value doesn't mean anything.  The simulator will update based on position later
        wind_sectors = new_wind_sectors;    // This list should be provided by the Json input data
    }

    // Meant to create small fluctuations in the wind pattern (needs to be fixed if used, presently only adds)
    public void rand_wind(){
        double dx = rand.nextGaussian();
        double dy = rand.nextGaussian();
        double dz = rand.nextGaussian();
        Point3D change = new Point3D(dx,dy,dz);
        wind = wind.add(change);
    }

    // This is called by the state.StateParam object holding the state.EnvParam
    // Called every timestep after the position of the quad has been updated.
    public void update_wind(Point3D new_pos){

        for(int i = 0; i < wind_sectors.size(); i++){
            AreaBound nextArea = wind_sectors.get(i);
            if(nextArea.isInBounds(new_pos)){
                wind = nextArea.getWind_pattern();
                return;
            }
        }
        wind = new Point3D(0, 0, 0);
        return;
    }

    @Override
    public String toString() {
        return "state.EnvParam{" +
                " windPattern : " + wind_sectors.toString() +
                '}';
    }

    // Getters and Setters
    public Point3D getWind() {
        return wind;
    }
    public void setWind(Point3D wind) {
        this.wind = wind;
    }

    public ArrayList<AreaBound> getWind_sectors() {
        return wind_sectors;
    }
}
