package state;

import javafx.geometry.Point3D;
import state.QuadParam;

/*
 state.StateParam.java
 Author: Taylor Welker
 June 4, 2018

 The purpose of this class is to hold info that is critical to preserving the state of the simulation.

 This class holds the state of the simulation at a particular point in time.  The state.QuadParam are parameters specific to
 the particular model of quadcopter, and are constant. The state.EnvParam are parameters specific to the environment (meaning
 the wind in this case).  The remaining parameters can all be modified by the program and represent the state it is in
 in terms of position, time, and remaining battery life.
*/
public class StateParam {

    private QuadParam quadParam;    // These are static parameters associated with the quadcopter model
    private EnvParam envParam;      // This holds parameters related to the environment (wind)
    private String flightMode;      // This tells us whether or not the quad is attempting to land
    private double lat, lon, alt, time, battery; // These define the state (lat and lon are in degrees, alt (m), time and battery (sec)
    private Point3D velocity;       // This holds the quadcopter's velocity in terms of a vector measured in m/s
    private double acceleration;    // This holds the quadcopter's constant acceleration in terms of a constant measured in m/s^2

    // Initialize the parameters
    public StateParam(QuadParam new_quadParam, EnvParam new_envParam, String new_flightMode, double new_time, double new_lat, double new_lon,
                      double new_alt, double new_battery, Point3D init_vel){

        quadParam = new_quadParam;
        envParam = new_envParam;
        flightMode = new_flightMode;
        lat = new_lat;
        lon = new_lon;
        alt = new_alt;
        time = new_time;
        battery = new_battery;
        velocity = init_vel;
        acceleration = 13.62215;    // For our simulations, we are assuming a constant acceleration for the quad.  See
                                    // the README for documentation describing how we obtained this value.
    }

    // This is called every timestep, and is used to change the wind parameter being used based on the new position
    // obtained from the previous timestep.
    public void updateWindWithPos(){
        envParam.update_wind(new Point3D(lon, lat, alt));
    }

    @Override
    public String toString() {
        return "state.StateParam{" +
                "quadParam :" + quadParam +
                ", envParam :" + envParam +
                ", flightMode : " + flightMode +
                ", lat : " + lat +
                ", lon : " + lon +
                ", alt : " + alt +
                ", time : " + time +
                ", battery : " + battery +
                ", velocity : " + velocity +
                ", acceleration : " + acceleration +
                '}';
    }

    // Getters and Setters
    public QuadParam getQuadParam() {
        return quadParam;
    }
    public String getFlightMode() {
        return flightMode;
    }
    public void setFlightMode(String flightMode) {
        this.flightMode = flightMode;
    }
    public double getLat() {
        return lat;
    }
    public void setLat(double lat) {
        this.lat = lat;
    }
    public double getLon() {
        return lon;
    }
    public void setLon(double lon) {
        this.lon = lon;
    }
    public double getAlt() {
        return alt;
    }
    public void setAlt(double alt) {
        this.alt = alt;
    }
    public double getTime() {
        return time;
    }
    public void setTime(double time) {
        this.time = time;
    }
    public double getBattery() {
        return battery;
    }
    public void setBattery(double battery) {
        this.battery = battery;
    }
    public Point3D getVelocity() {
        return velocity;
    }
    public void setVelocity(Point3D velocity) {
        this.velocity = velocity;
    }
    public double getAcceleration() {
        return acceleration;
    }
    public EnvParam getEnvParam() {
        return envParam;
    }
}


