package state;/*
 state.QuadParam.java
 Author: Taylor Welker
 June 4, 2018

 The purpose of this class is to hold info that is critical to preserving the state of the simulation (specifically
 static parameters associated with the quadcopter).

 This class is used as a sub-class to the simulation-state-saving state.StateParam class.  This one takes care of the
 parameters specific to the quadcopter model, and things generally related to its dynamics.  These parameters don't
 change, and act like constants for the quad.
*/


public class QuadParam{

    // sim.Simulation specific variables
    private String quad_id;         // Unique identify for the quadcopter
    private String model;           // Each model of quad has different specifications

    // Model specific variables
    private double quad_weight;     // In kg
    private double max_hztl_spd;    // In m/s
    private double max_vrtl_spd;    // "
    private double battery_lim;     // In seconds

    // Initialize the parameters associated with the quadcopter based on the model type.
    public QuadParam(String new_quad_id, String new_model){
        quad_id = new_quad_id;
        model = new_model;
        if(model.equals("solo")){
            quad_weight = 1.5;
            max_hztl_spd = 24.5872;
            max_vrtl_spd = 7.0;
            battery_lim = 1500.0;
        }
        else if (model.equals("dji_phantom_4")){
            quad_weight = 1.5;
            max_hztl_spd = 20.1168;
            max_vrtl_spd = 6.0;
            battery_lim = 1800.0;
        }
        else {
            quad_weight = 1.282;
            max_hztl_spd = 17.8816;
            max_vrtl_spd = 5.0;
            battery_lim = 1200.0;
        }
    }

    @Override
    public String toString() {
        return "state.QuadParam{" +
                "quad_id='" + quad_id + '\'' +
                ", model='" + model + '\'' +
                ", quad_weight=" + quad_weight +
                ", max_hztl_spd=" + max_hztl_spd +
                ", max_vrtl_spd=" + max_vrtl_spd +
                ", battery_lim=" + battery_lim +
                '}';
    }

    // Getters
    public String getQuad_id() {
        return quad_id;
    }
    public String getModel() {
        return model;
    }
    public double getQuad_weight() {
        return quad_weight;
    }
    public double getMax_hztl_spd() {
        return max_hztl_spd;
    }
    public double getMax_vrtl_spd() {
        return max_vrtl_spd;
    }
    public double getBattery_lim() {
        return battery_lim;
    }
}