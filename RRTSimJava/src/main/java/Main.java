import gazebo_world_gen.GazWorldGen;
import sim.Simulation;

import java.io.FileWriter;

/*
 Main.java
 Author: Taylor Welker
 June 4, 2018

 The purpose of this class is to execute simulator test cases, and is not meant to be used by the RRT planner.

 The Main class takes in 5 arguments:
 -initial_state.txt path: this is the state the quadcopter starts in
 -desired_state.txt path: this is the state the quadcopter wants to get to
 -output.txt path:        this is the actual final state the simulation calculates
 -preset_mission_number:  this is used if a preset mission should be used instead of the txt files (overrides the
                          first two arguments if not "None"
 -display switch:         if "true", the simulation will provide a plot describing the quad's path through the simulation.

 Note that all 5 arguments are required to run the test, but the first two .txt files will not be considered if the
 preset_mission_number is something other than "None".  The preset missions can be found in the sim.TransitionFunctions.java
 files.
*/
public class Main {

    public static void main(String [] args){

        // sim.Simulation is the class that controls the simulation.  Main simply creates and calls its methods.
        // The RRT planner should do the same, but instead of calling "single_test", it should call "runSim"
        Simulation sim = new Simulation();

        sim.single_test(args[0], args[1], args[2], args[3], args[4]);
        GazWorldGen gazWorldGen = new GazWorldGen("/usr/share/gazebo-9/worlds/uofu.stl","/usr/share/gazebo-9/worlds/uofu.dae", 1);
    }
}
