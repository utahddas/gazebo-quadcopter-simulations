package sim;

import javafx.geometry.Point3D;
import state.StateParam;

/*
 sim.Translation.java
 Author: Taylor Welker
 June 4, 2018

 The purpose of this class is to hold methods meant to perform conversions and more complex mathematical calculations.

 This class can:
 1. find distances between GPS coordinates and convert them to 3D vectors measured in meters
 2. add such a vector back to another GPS coordinate and obtain the resulting GPS location
 3. compute the optimal velocity vector that the quadcopter should follow to counteract crosswind.
*/
public class Translation {

    double EARTH_RADIUS = 6378137.0;

    public Point3D get_GPS_vector(StateParam initState, StateParam destState){

        double x, y;
        double threshold = 0.000001;    // This is the threshold that declares the lat and lon coordinates to be close enough to ignore any differences.

        // If the lat and lon between the init state and the dest state are close enough, simply stay at the same lat and lon
        if((Math.abs(initState.getLat() - destState.getLat()) < threshold) && (Math.abs(initState.getLon() - destState.getLon()) < threshold)){
            x = 0;
            y = 0;
        }

        else{

            // We will use these two sides of the triangle and the angle between them to find the 3rd side (the vector)
            double b = EARTH_RADIUS + destState.getAlt();
            double c = EARTH_RADIUS + initState.getAlt();

            // Find b^2 and c^2 and 2*b*c
            double b2 = b*b;
            double c2 = c*c;
            double bc2 = 2*b*c;

            // Longitudinal calculations
            double alpha = destState.getLon() - initState.getLon(); // Find the angle between them (alpha = angle)
            alpha = alpha * Math.PI / 180.0;                        // Convert angle to radians
            double cosine = 1 - (alpha * alpha / 2);                // Find the cosine between them (done with small angle approx)
            x = Math.sqrt(b2 + c2 - bc2*cosine);                    // Compute the vector in the x direction (law of cosines)

            // Be sure to keep the negative if it was there previously
            if (alpha < 0){
                x = -x;
            }

            // Latitudinal calculations
            alpha = destState.getLat() - initState.getLat(); // Find the angle between them (alpha = angle)
            alpha = alpha * Math.PI / 180.0;                 // Convert angle to radians
            cosine = 1 - (alpha * alpha / 2);                // Find the cosine between them (done with small angle approx)
            y = Math.sqrt(b2 + c2 - bc2*cosine);             // Compute the vector in the y direction (law of cosines)

            // Be sure to keep the negative if it was there previously
            if (alpha < 0){
                y = -y;
            }

        }

        double z = destState.getAlt() - initState.getAlt(); // Compute the vector in the z direction
        return new Point3D(x,y,z);                          // Return the resulting vector
    }

    /*
     This process has been confirmed by two separate stack overflow pages.
     One thing to note: Math.cosassumes argument is in radians, so convert lat to radians.
     https://stackoverflow.com/questions/7477003/calculating-new-longitude-latitude-from-old-n-meters
    */
    public StateParam add_to_GPS(StateParam initState, Point3D pos_vec){
        double lat = initState.getLat() + (180.0/Math.PI) * (pos_vec.getY() / EARTH_RADIUS);
        double lon = initState.getLon() + (180.0/Math.PI) * (pos_vec.getX() / EARTH_RADIUS);
        initState.setLat(lat);
        initState.setLon(lon);
        return initState;
    }

    public Point3D find_crosswind_spd(Point3D dist, Point3D wind, double S){
        // Solve for theta_s (vector dot product rule)
        double theta_s = Math.acos((wind.dotProduct(dist))/(wind.magnitude()*dist.magnitude()));

        // Solve for theta_w (Law of Sines)
        double theta_w = Math.asin(wind.magnitude()*(Math.sin(theta_s)/S));

        // Solve for theta_d (180 deg triangle rule)
        double theta_d = Math.PI - theta_s - theta_w;

        // Solve for D (Law of Sines)
        double D = Math.sin(theta_d) * (S / Math.sin(theta_s));

        // Solve for d_vec (dist_vec / ||dist_vec|| * D)
        Point3D d_vec = (dist.multiply(1 / dist.magnitude())).multiply(D);

        // Return s_vec (s_vec = d_vec - w_vec)
        Point3D s_vec = d_vec.subtract(wind);

        return s_vec;
    }
}
