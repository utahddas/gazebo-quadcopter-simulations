package sim;

import javafx.geometry.Point3D;
import state.AreaBound;
import state.EnvParam;
import state.QuadParam;
import state.StateParam;

import java.util.ArrayList;

/*
 sim.TransitionFunctions.java
 Author: Taylor Welker
 June 4, 2018

 The purpose of this class is to provide methods to calculate state transitions for each timestep.

 The primary purpose of this class is to update the velocity and position of the quadcopter at each timestep, as well as
 to check its state and determine if the simulation has met the required conditions to terminate.

 The simulation will terminate if either one of the following conditions are met:
 1. The quacopter is within a 1m distance from its objective
 2. The quadcopter's remaining battery power is less than 10% of its maximum limit.

 In addition to updating the velocity and position of the quadcopter at each timestep, this class contains other methods
 that are used to initialize the preset mission test cases if required. These methods include:
 -initialize_Models
 -gen_windmap
 -preset_Test
*/
public class TransitionFunctions {

    static double time_penalty;
    static double last_dist;

    TransitionFunctions(){
        time_penalty = 0;
        last_dist = 0;
    }

    /*
    This function parses the JSON from our input files.
    The first file holds the current state, and the second holds the desired state.
    */
    public ArrayList<StateParam> initialize_Models(String init_input_file, String final_input_file){
        JsonStateEncoder jsonStateEncoder = new JsonStateEncoder();
        ArrayList<StateParam> states = new ArrayList<StateParam>();
        states.add(jsonStateEncoder.read_Json_file(init_input_file));
        states.add(jsonStateEncoder.read_Json_file(final_input_file));
        return states;
    }

    // This function returns a custom windmap corresponding to the preset mission number
    public ArrayList<AreaBound> gen_windmap(int mission_num){

        // The windmap is a list of AreaBounds describing the wind throughout the environment. This is what will be returned
        ArrayList<AreaBound> wind_sectors = new ArrayList<>();

        // These are default filler values if the mission number is incorrect
        AreaBound universal_area = new AreaBound(-180.0, 180.0, -90.0, 90.0,
                0,5.0, new Point3D(0, 0, 0));


        if(mission_num == 1){
            // Describe the area using state.AreaBound objects
            AreaBound area_0 = new AreaBound(-180.0, 180.0, -90.0, 90.0,
                    0,5.0, new Point3D(0, 0, -5.0));
            AreaBound area_1 = new AreaBound(-180.0, 180.0, -90.0, 90.0,
                    5.0, 100.0, new Point3D(0, 0, 5.0));

            // Add the wind to the list
            wind_sectors.add(area_0);
            wind_sectors.add(area_1);
        }
        else if(mission_num == 2){
            // Describe the area using state.AreaBound objects
            AreaBound area_0 = new AreaBound(-0.002, -0.001, 0.001, 0.002,
                    0,100.0, new Point3D(0, -20.0, 0.0));
            AreaBound area_1 = new AreaBound(-0.004, -0.002, 0.002, 0.004,
                    0,100.0, new Point3D(0, 20.0, 0.0));
            AreaBound area_2 = new AreaBound(-0.006, -0.004, 0.004, 0.006,
                    0,100.0, new Point3D(0, 0, -10.0));

            // Add the wind to the list
            wind_sectors.add(area_0);
            wind_sectors.add(area_1);
            wind_sectors.add(area_2);
        }
        else if(mission_num == 6){
            wind_sectors.add(universal_area);
        }
        else{
            wind_sectors.add(universal_area);
        }
        return wind_sectors;
    }

    // If the user has opted to run a "pretest mission, then this will set it up with the corresponding parameters
    public ArrayList<StateParam> preset_Test(String mission_num){
        ArrayList<StateParam> states = new ArrayList<StateParam>();

        int number = Integer.parseInt(mission_num);

        String id = new String("iris1");
        String model = new String("iris+");

        QuadParam quadParam = new QuadParam(id, model);
        ArrayList<AreaBound> wind_map = gen_windmap(number);
        EnvParam envParam = new EnvParam(wind_map);

        if(number == 1){
            // Vertical takeoff test
            states.add(new StateParam(quadParam, envParam, "land", 0.0, 0.0, 0.0, 0.0, quadParam.getBattery_lim(), new Point3D(0.0, 0.0, 0.0)));
            states.add(new StateParam(quadParam, envParam, "fly", states.get(0).getTime(), 0.0, 0.0, 10.0, states.get(0).getBattery(),new Point3D(0.0, 0.0, 0.0)));
        }
        else if(number == 2){
            // Long distance test
            states.add(new StateParam(quadParam, envParam, "fly", 0.0, 0.0, 0.0, 10.0, quadParam.getBattery_lim(), new Point3D(0.0, 0.0, 0.0)));
            states.add(new StateParam(quadParam, envParam, "fly", states.get(0).getTime(), 0.005, -0.005, 10.0, states.get(0).getBattery(), new Point3D(0.0, 0.0, 0.0)));
        }
        else if(number == 3){
            // Same spot test
            states.add(new StateParam(quadParam, envParam, "fly", 0.0, 10.0, -200.0, 40.0, quadParam.getBattery_lim(), new Point3D(0.0, 0.0, 0.0)));
            states.add(new StateParam(quadParam, envParam, "fly", states.get(0).getTime(), 10.0, -200.0, 40.0, states.get(0).getBattery(), new Point3D(0.0, 0.0, 0.0)));
        }
        else if(number == 4){
            // Close enough test
            states.add(new StateParam(quadParam, envParam, "fly", 0.0, 30.0, -5.0, 10.0, quadParam.getBattery_lim(), new Point3D(0.0, 0.0, 0.0)));
            states.add(new StateParam(quadParam, envParam, "fly", states.get(0).getTime(), 30.000001, -5.000001, 10.0, states.get(0).getBattery(), new Point3D(0.0, 0.0, 0.0)));
        }
        else if(number == 5){
            // Landing test
            states.add(new StateParam(quadParam, envParam,"fly", 0.0, 0.0, 0.0, 10.0, quadParam.getBattery_lim(), new Point3D(0.0, 0.0, 0.0)));
            states.add(new StateParam(quadParam, envParam,"land", states.get(0).getTime(), 0.0, 0.0, 0.0, states.get(0).getBattery(), new Point3D(0.0, 0.0, 0.0)));
        }
        else{
            // 36 m real world test
            states.add(new StateParam(quadParam, envParam, "fly", 0.0, 40.768994, -111.844509, 10.0, quadParam.getBattery_lim(), new Point3D(0.0, 0.0, 0.0)));
            states.add(new StateParam(quadParam, envParam, "fly", states.get(0).getTime(), 40.768679, -111.844384, 10.0, states.get(0).getBattery(), new Point3D(0.0, 0.0, 0.0)));
        }

        return states;
    }

    // This method updates the velocity of the quad based on acceleration, position, and the wind velocity
    public Point3D updateVel(StateParam initState, StateParam finalState, double accel, double step_size){

        double max_speed = initState.getQuadParam().getMax_hztl_spd();

        Translation translation = new Translation();
        Point3D distance_vector = translation.get_GPS_vector(initState,finalState); // Find the distance vector (x,y,z) = (along lon, along lat, alt)
        double distance = distance_vector.magnitude();                              // Calc the distance remaining

        // Update the penalty if the quad hasn't changed is position enough
        if(Math.abs(distance - getLast_dist()) < 0.5){ setTime_penalty(getTime_penalty() + step_size); }

        // Set the last distance so we can update the penalty again if needed
        setLast_dist(distance);

        // Get the critical velocities
        Point3D init_vel = initState.getVelocity();                                 // The current quad velocity
        Point3D wind_vel = initState.getEnvParam().getWind();                       // The current wind velocity
        Point3D delta_vel = distance_vector.multiply(1/distance_vector.magnitude()).multiply(accel*step_size);  // The predicted effective velocity this timestep (wind not included)

        // If we are within a distance of (v^2)/(2a), then we need to slow down rather than speed up. The extra terms are meant to incorporate the wind
        if(distance < (Math.pow(init_vel.magnitude(),2) / (2 * accel)) - initState.getEnvParam().getWind().magnitude() - getTime_penalty()){
            init_vel = init_vel.subtract(delta_vel);
        }
        else{
            // Accelerate if we haven't already reached our top speed.
            if(init_vel.magnitude() < max_speed) {
                init_vel = init_vel.add(delta_vel);
            }
        }

        /*
        There are 3 conditions to use our crosswind trigonometry method:
        1. The wind velocity magnitude must be less than the quadcopter's velocity
        2. The wind must not be at a standstill
        3. The wind and the quadcopter velocities must not be parallel
        */
        if((wind_vel.magnitude() < init_vel.magnitude()) && (wind_vel.magnitude() != 0) && (init_vel.crossProduct(wind_vel).magnitude() != 0)){
            init_vel = translation.find_crosswind_spd(distance_vector, wind_vel, init_vel.magnitude());
        }

        // Print its true velocity, but return its believed velocity (independent of wind)
        // System.out.println("distance: " + Double.toString(distance));
        // System.out.println("normal: " + Double.toString(init_vel.magnitude()));
        // System.out.println();
        return init_vel;
    }


    /*
     This function returns a boolean signaling whether or not the mission is over.
     It checks whether or not we have reached our destination, and the battery level to determine it.
    */
    public boolean is_MissionOver(StateParam initState, StateParam finalState){
        if(is_DestReached(initState, finalState) || is_BattEmpty(initState.getBattery(), initState.getQuadParam().getBattery_lim())){
            return true;
        }
        else{
            return false;
        }
    }

    /*
     This function checks to see if the quad has reached its destination.
     If it is within an error distance, we will consider the quad to be "at its destination"
    */
    private boolean is_DestReached(StateParam initState, StateParam finalState){

        // Get the distance vector, and its magnitude
        Translation translation = new Translation();
        Point3D distance_vector = translation.get_GPS_vector(initState, finalState);
        double distance = distance_vector.magnitude();

        // If the distance is less than a meter, then we have practically arrived
        if(distance < (1.0)){ return true; }
        else{ return false; }
    }

    /*
     This function checks to see if the battery is too low to continue
     It should only fly 90% of its max flight time.
    */
    private boolean is_BattEmpty(double current_bat, double battery_limit){

        // Return true if battery is less than 10% of its limit
        if((current_bat/battery_limit) < .1){ return true; }
        else{ return false; }
    }

    /*
     This function updates the position of the quadcopter at every timestep
     1. It finds the distance between where it is and where it wants to be
     2. It computes the distance it can travel in a single timestep and scales the distance vector accordingly
     3. It uses the scaled distance vector to obtain a new lat, lon and alt (position)
    */
    public StateParam update_pos(StateParam initState, double step_size){

        Translation translation = new Translation();        // Allows us to convert distance to GPS
        Point3D wind = initState.getEnvParam().getWind();   // Get the wind vector

        // The distance the quad traveled in the time step is its (velocity + wind velocity) * step_size
        Point3D distance_traveled = initState.getVelocity().add(wind).multiply(step_size);

        initState = translation.add_to_GPS(initState, distance_traveled);   // Convert newPos_vector back into GPS
        if((initState.getAlt() + distance_traveled.getZ()) < 0){            // We can't move beneath the earth
            initState.setAlt(0.0);
        }
        else{
            initState.setAlt(initState.getAlt() + distance_traveled.getZ());    // Set initState with updated Altitude
        }
        return initState;
    }

    // Getters and Setters
    public static double getTime_penalty() {
        return time_penalty;
    }
    public static double getLast_dist() {
        return last_dist;
    }
    public static void setLast_dist(double last_dist) {
        TransitionFunctions.last_dist = last_dist;
    }
    public static void setTime_penalty(double time_penalty) {
        TransitionFunctions.time_penalty = time_penalty;
    }

}
