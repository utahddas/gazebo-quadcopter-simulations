package sim;

import display.Display3D;
import org.json.JSONObject;
import state.StateParam;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Random;

/*
 sim.Simulation.java
 Author: Taylor Welker
 June 4, 2018

 The purpose of this class is to execute the simulation.  Called by the Main.java file and the RRT planner.

 This class's main feature is its "runSim()" method.  It simply takes in the quadcopter's initial state, and the desired
 final state, and runs the simulation, returning the result as a JSONObject.  The Main.java class interfaces with the
 runSim method by first calling the "single_test()" method.  This simply parses the input arguments, and then, upon
 receiving the JSONObject from the runSim method, prints its contents to a txt file (specified by the output_filename
 argument).

 The RRT planner should only have to call the "runSim()" method.
*/

public class Simulation {

    // This is only called if there is not RRT planner.  It simply decides whether to use the mission provided by the
    // filenames, or to follow a pre-programmed mission, and then calls runSim.
    public void single_test(String init_input, String final_input, String output_filename, String map, String display){

        TransitionFunctions transitionFunctions = new TransitionFunctions();

        // This holds the states received from the planner (init and final states)
        ArrayList<StateParam> states;

        // Populate 'states' with either the mission described by the files, or a preprogrammed mission
        if(!map.equals("None")){
            states = transitionFunctions.preset_Test(map);
        }
        else{
            states = transitionFunctions.initialize_Models(init_input, final_input);
        }

        // Call runSim to run the simulation
        JSONObject obj = runSim(states.get(0), states.get(1));

        try{
            FileWriter fileWriter = new FileWriter(output_filename);
            fileWriter.write(obj.toString());
            fileWriter.close();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    // This is the main event.  This method actually executes the simulation given these parameters
    public static JSONObject runSim(StateParam initState, StateParam finalState){
        TransitionFunctions transitionFunctions = new TransitionFunctions();

        // A couple of preset variables. "step_size" can be changed to suit the user's preferences.
        double step_size = 0.1;
        double time_passed = 0;

        // Set the wind using the state.StateParam's built in function
        initState.updateWindWithPos();

        // Print the original state to the terminal
        System.out.println("Initial State:  ");
        System.out.println(initState.toString());

        // Initialize the 3D display.Display object, and the random number generator
        Display3D display1 = new Display3D(finalState);
        display1.add_to_waypoints(initState);
        Random random = new Random();

        // This is where most of our simulation occurs.   This updates the state until the mission is over
        while((!transitionFunctions.is_MissionOver(initState, finalState))){

            // Update the velocity, position and wind models
            time_passed += step_size;                                                                                            // Update the current time in the simulation
            initState.setVelocity(transitionFunctions.updateVel(initState, finalState, initState.getAcceleration(), step_size)); // Update the vel before position
            initState = transitionFunctions.update_pos(initState, step_size);                                                    // Update the quadcopter's position
            initState.updateWindWithPos();                                                                                       // Update the wind based on our new position

            // Update the battery
            double sample = (random.nextGaussian()*.2 + 1)*step_size;   // Grab a random sample centered at 1*step_size and with a st dev of .2*step_size
            initState.setBattery(initState.getBattery() - sample);      // Update the current battery level

            // Update the display
            display1.add_to_waypoints(initState);   // Add the current state to our final plot
        }

        // As long as the simulation wasn't trivial, plot it
        if(display1.getWaypoints().size() > 0){ display1.plot_path(finalState);}

        // At the end, add the total simulation time to the initState.time
        initState.setTime(time_passed);

        // Print the final state to the terminal
        System.out.println("Final State:  ");
        System.out.println(initState.toString());

        // Write the final state to a JSON file.
        JsonStateEncoder encoder = new JsonStateEncoder();
        JSONObject obj = encoder.write_JSON(initState);

        // Return the JSONObject describing the ending state
        return obj;
    }
}
