package sim;

import javafx.geometry.Point3D;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import state.AreaBound;
import state.EnvParam;
import state.QuadParam;
import state.StateParam;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

/*
 sim.JsonStateEncoder.java
 Author: Taylor Welker
 June 4, 2018

 The purpose of this class is to encode/decode JSON data to enable communication with the RRT planner

 This class contains two nearly identical methods to decode JSON data into the simulation input parameters, one to
 encode the final simulation state into output for the RRT planner, and one to read JSON strings from .txt files.

 The two decoding methods are "read_Json_file" and "read_Json".  Their outputs are the same, but "read_Json_file" takes
 a txt file as an argument, while the "read_Json" method takes a JSONObject as its input.  The former is meant for
 testing the simulator using txt files, while the second is meant to be used as the interface for the RRT planner to
 pass its JSON information without needing to create a txt file first.

 It is critical that the JSON files have the correct format as both the encoder and decoder use this format, and have
 been designed specifically for it.  Any changes to the JSON structure will necessitate changes in the encoder and
 decoders.
*/
public class JsonStateEncoder {

    // This is used to get the JSON string from a given file
    public static String readFile(String filename) {
        String result = "";
        try{
            BufferedReader br = new BufferedReader(new FileReader(filename));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while(line != null){
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // This is used to parse Json files into java objects
    public StateParam read_Json_file(String filename){
        StateParam parameters = null;

        try {
            // Get the three major JSON objects, the state.StateParam, the state.QuadParam, and the state.EnvParam
            JSONObject obj = new JSONObject(readFile(filename));
            JSONObject quad_param = obj.getJSONObject("quad_param");
            org.json.JSONArray env_param = obj.getJSONArray("env_param");

            // Parse the env_param with care, storing the new AreaBounds in a list
            ArrayList<AreaBound> wind_sectors = new ArrayList<>();
            for(Object j : env_param){
                JSONObject object = (JSONObject) j;
                JSONObject newArea = object.getJSONObject("state.AreaBound");
                double min_lon = newArea.getDouble("min_lon");
                double max_lon = newArea.getDouble("max_lon");
                double min_lat = newArea.getDouble("min_lat");
                double max_lat = newArea.getDouble("max_lat");
                double min_alt = newArea.getDouble("min_alt");
                double max_alt = newArea.getDouble("max_alt");
                JSONObject wind_raw = newArea.getJSONObject("wind");
                double wind_x = wind_raw.getDouble("x");
                double wind_y = wind_raw.getDouble("y");
                double wind_z = wind_raw.getDouble("z");
                Point3D wind = new Point3D(wind_x, wind_y, wind_z);
                AreaBound parsedArea = new AreaBound(min_lon, max_lon, min_lat, max_lat, min_alt, max_alt, wind);
                wind_sectors.add(parsedArea);
            }

            // Use the list of state.AreaBound objects to create the envParam object
            EnvParam envParam = new EnvParam(wind_sectors);

            // Parse the required quad_param items to recreate it in Java
            String quad_id = quad_param.getString("quad_id");
            String model = quad_param.getString("model");
            QuadParam quadParam = new QuadParam(quad_id, model);

            // Parse the remaining items
            JSONObject init_vel = obj.getJSONObject("velocity");
            double x = init_vel.getDouble("x");
            double y = init_vel.getDouble("y");
            double z = init_vel.getDouble("z");
            String flight_mode = obj.getString("flight_mode");
            double time = obj.getDouble("time");
            double lat = obj.getDouble("lat");
            double lon = obj.getDouble("lon");
            double alt = obj.getDouble("alt");
            double battery = obj.getDouble("battery");
            Point3D velocity = new Point3D(x,y,z);

            //Given these items, recreate the corresponding state
            parameters = new StateParam(quadParam, envParam, flight_mode, time, lat, lon, alt, battery, velocity);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Return the state.StateParam object
        return parameters;
    }

    public StateParam read_Json(JSONObject obj){
        StateParam parameters = null;

        try{
            JSONObject quad_param = obj.getJSONObject("quad_param");
            org.json.JSONArray env_param = obj.getJSONArray("env_param");

            // Parse the env_param with care, storing the new AreaBounds in a list
            ArrayList<AreaBound> wind_sectors = new ArrayList<>();
            for(Object j : env_param){
                JSONObject object = (JSONObject) j;
                JSONObject newArea = object.getJSONObject("state.AreaBound");
                double min_lon = newArea.getDouble("min_lon");
                double max_lon = newArea.getDouble("max_lon");
                double min_lat = newArea.getDouble("min_lat");
                double max_lat = newArea.getDouble("max_lat");
                double min_alt = newArea.getDouble("min_alt");
                double max_alt = newArea.getDouble("max_alt");
                JSONObject wind_raw = newArea.getJSONObject("wind");
                double wind_x = wind_raw.getDouble("x");
                double wind_y = wind_raw.getDouble("y");
                double wind_z = wind_raw.getDouble("z");
                Point3D wind = new Point3D(wind_x, wind_y, wind_z);
                AreaBound parsedArea = new AreaBound(min_lon, max_lon, min_lat, max_lat, min_alt, max_alt, wind);
                wind_sectors.add(parsedArea);
            }

            // Use the list of state.AreaBound objects to create the envParam object
            EnvParam envParam = new EnvParam(wind_sectors);

            // Parse the required quad_param items to recreate it in Java
            String quad_id = quad_param.getString("quad_id");
            String model = quad_param.getString("model");
            QuadParam quadParam = new QuadParam(quad_id, model);

            // Parse the remaining items
            JSONObject init_vel = obj.getJSONObject("velocity");
            double x = init_vel.getDouble("x");
            double y = init_vel.getDouble("y");
            double z = init_vel.getDouble("z");
            String flight_mode = obj.getString("flight_mode");
            double time = obj.getDouble("time");
            double lat = obj.getDouble("lat");
            double lon = obj.getDouble("lon");
            double alt = obj.getDouble("alt");
            double battery = obj.getDouble("battery");
            Point3D velocity = new Point3D(x,y,z);

            //Given these items, recreate the corresponding state
            parameters = new StateParam(quadParam, envParam, flight_mode, time, lat, lon, alt, battery, velocity);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Return the state.StateParam object
        return parameters;
    }

    public JSONObject write_JSON(StateParam finalState){

        // Create the primary JSON objects
        JSONObject obj = new JSONObject();      // This is the one that will be returned
        JSONObject q_obj = new JSONObject();    // This represents the state.QuadParam object, and will be added to obj
        JSONArray e_obj = new JSONArray();      // This represents the state.EnvParam object, and will be added to obj
        try {
            // Grab the State variables to save time accessing their elements
            QuadParam quadParam = finalState.getQuadParam();
            EnvParam envParam = finalState.getEnvParam();

            // Write objects to quad_param
            q_obj.put("quad_id", quadParam.getQuad_id());
            q_obj.put("model", quadParam.getModel());
            q_obj.put("max_hztl_spd", quadParam.getMax_hztl_spd());
            q_obj.put("max_vrtl_spd", quadParam.getMax_vrtl_spd());
            q_obj.put("quad_weight", quadParam.getQuad_weight());
            q_obj.put("battery_lim", quadParam.getBattery_lim());

            // Iterate through each of the state.AreaBound wind sectors and add them to the JSON
            for(int i = 0; i < envParam.getWind_sectors().size(); i++){
                JSONObject a_obj = new JSONObject();
                JSONObject wind_obj = new JSONObject();
                AreaBound currentArea = envParam.getWind_sectors().get(i);
                a_obj.put("min_lon", currentArea.getMin_lon());
                a_obj.put("max_lon", currentArea.getMax_lon());
                a_obj.put("min_lat", currentArea.getMin_lat());
                a_obj.put("max_lat", currentArea.getMax_lat());
                a_obj.put("min_alt", currentArea.getMin_alt());
                a_obj.put("max_alt", currentArea.getMax_alt());

                // Create another separate object to hold wind parameters and add it to the state.AreaBound object
                Point3D wind = currentArea.getWind_pattern();
                wind_obj.put("x", wind.getX());
                wind_obj.put("y", wind.getY());
                wind_obj.put("z", wind.getZ());
                a_obj.put("wind", wind_obj);

                // Add a label to the state.AreaBound object so our input parser can correctly interpret the data.
                JSONObject label = new JSONObject();
                label.put("state.AreaBound", a_obj);
                e_obj.put(i, label);
            }

            // Add the sub classes to the state.StateParam
            obj.put("quad_param", q_obj);
            obj.put("env_param", e_obj);

            // Finally, add the normal state.StateParam variables
            obj.put("flight_mode", finalState.getFlightMode());
            obj.put("lat", finalState.getLat());
            obj.put("lon", finalState.getLon());
            obj.put("alt", finalState.getAlt());
            obj.put("time", finalState.getTime());
            obj.put("battery", finalState.getBattery());

            // We need to create a variable for the velocity object
            JSONObject v_obj = new JSONObject();
            v_obj.put("x", finalState.getVelocity().getX());
            v_obj.put("y", finalState.getVelocity().getY());
            v_obj.put("z", finalState.getVelocity().getZ());
            obj.put("velocity", v_obj);

        } catch (JSONException e) {
            System.out.println("Error writing output to JSON object");
            e.printStackTrace();
        }
        System.out.print(obj.toString());
        return obj;
    }
}