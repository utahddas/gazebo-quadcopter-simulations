package gazebo_world_gen;


public class GazWorldGen {

    String stl_filepath;
    String dae_filepath;
    int num_quads;
    int square_length;
    int startx = 79;
    int starty = -37;
    int alt = 8;
    //double startlat = 40.768069;
    //double startlon = -111.845187;
    double startlat = 40.768012;
    double startlon = -111.845230;
    double EARTH_RADIUS = 6378137.0;

    public GazWorldGen(String new_stl, String new_dae, int new_num_quads){
        stl_filepath = new_stl;
        dae_filepath = new_dae;
        num_quads = new_num_quads + 1;
        if(Math.sqrt(num_quads)%1 == 0){square_length = (int) Math.sqrt(num_quads);}
        else{square_length = (int) Math.sqrt(num_quads) + 1;}

    }

    private double findDistanceOffset(double old_coord, double new_coord, double old_alt, double new_alt){

        double x;
        double threshold = 0.000001;        // This is the threshold that declares the lat and lon coordinates to be close enough to ignore

        if(Math.abs(old_coord - new_coord) < threshold){
            x = 0;
        }

        else{

            // We will use the two sides of the triangle and the angle between them to find the 3rd side (the vector)
            double b = EARTH_RADIUS + new_alt;
            double c = EARTH_RADIUS + old_alt;

            // Find b^2 and c^2 and 2*b*c
            double b2 = b*b;
            double c2 = c*c;
            double bc2 = 2*b*c;

            // Calculations
            double alpha = new_coord - old_coord; // Find the angle between them (alpha = angle)
            alpha = alpha * Math.PI / 180.0;      // Convert angle to radians
            double cosine = 1 - (alpha * alpha / 2); // Find the cosine between them (done with small angle approx)
            x = Math.sqrt(b2 + c2 - bc2*cosine);    // Compute the vector in the given direction (law of cosines)

            if (alpha < 0) {
                x = -x;
            }
        }
        return x;
    }

    public String buildWorld(){
        StringBuilder sb = new StringBuilder();
        sb.append(buildExposition());
        sb.append(buildMap());
        for(int i = 0; i < num_quads; i++){
            sb.append(buildIris(i));
        }
        sb.append(buildWindPlugin());
        sb.append(buildConclusion());
        return sb.toString();
    }

    public String buildExposition(){
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\"?>\n");
        sb.append("<sdf version=\"1.4\">\n");
        sb.append("  <world name=\"default\">\n");
        sb.append("    <include>\n");
        sb.append("      <uri>model://sun</uri>\n");
        sb.append("    </include>\n");
        sb.append("    <light type=\"directional\" name=\"my_light\" cast_shadows=\"true\">\n");
        sb.append("      <origin pose=\"0 0 30 0 0 0\"/>\n");
        sb.append("      <diffuse rgba=\".9 .9 .9 1\"/>\n");
        sb.append("      <specular rgba=\".1 .1 .1 1\"/>\n");
        sb.append("      <attenuation range=\"20\"/>\n");
        sb.append("      <direction xyz=\"0 0 -1\"/>\n");
        sb.append("    </light>\n");

        return sb.toString();
    }

    public String buildMap(){
        StringBuilder sb = new StringBuilder();
        sb.append("    <model name=\"map\">\n");
        sb.append("      <pose>0 0 0 0 0 0</pose>\n");
        sb.append("      <static>true</static>\n");
        sb.append("      <link name=\"body\">\n");
        sb.append("        <collision name=\"collision\">\n");
        sb.append("          <geometry>\n");
        sb.append("            <mesh><uri>file://"+stl_filepath+"</uri></mesh>\n"); // Modifiable
        sb.append("          </geometry>\n");
        sb.append("        </collision>\n");
        sb.append("        <visual name=\"visual\">\n");
        sb.append("          <geometry>\n");
        sb.append("            <mesh><uri>file://"+dae_filepath+"</uri></mesh>\n"); // Modifiable
        sb.append("          </geometry>\n");
        sb.append("        </visual>\n");
        sb.append("      </link>\n");
        sb.append("    </model>\n");
        return sb.toString();
    }

    public String buildIris(int i){
        StringBuilder sb = new StringBuilder();
        sb.append("    <model name=\"iris"+String.valueOf(i)+"\">\n"); // Modifiable
        sb.append("      <enable_wind>true</enable_wind>\n");
        sb.append("      <include>\n");
        int offset = 2 + i*10;
        String portNum;
        if(offset == 2){portNum = "002";}
        else if(offset < 100){portNum = "0"+String.valueOf(offset);}
        else{portNum = String.valueOf(offset);}
        sb.append("        <uri>model://iris_with_standoffs_demo_9"+portNum+"</uri>\n"); // Modifiable
        sb.append("      </include>\n");

        double UofU2lon = -111.845487;
        double UofU2lat = 40.767980;

        double offlon = findDistanceOffset(UofU2lon, startlon, 0, alt);
        double offlat = findDistanceOffset(UofU2lat, startlat, 0, alt);

        //int nextx = startx + i%square_length;
        //int nexty = starty + Math.floorDiv(i, square_length);

        System.out.println("xoff: " + offlon);
        System.out.println("yoff: " + offlat);

        double nexty = -offlon + i%square_length;
        double nextx = offlat + Math.floorDiv(i, square_length);

        System.out.println("nextx: " + nextx);
        System.out.println("nexty: " + nexty);

        sb.append("      <pose>"+String.valueOf(nextx)+" "+String.valueOf(nexty)+" "+String.valueOf(alt)+" 0 0 0</pose>\n"); // Modifiable
        //sb.append("      <pose>"+0+" "+8+" "+String.valueOf(alt)+" 0 0 0</pose>\n"); // Modifiable

        sb.append("    </model>\n");
        return sb.toString();
    }

    public String buildWindPlugin(){
        StringBuilder sb = new StringBuilder();
        sb.append("    <wind>\n");
        sb.append("      <linear_velocity>0 0 0</linear_velocity>\n"); // Modifiable
        sb.append("    </wind>\n");
        sb.append("    <plugin name=\"wind\" filename=\"libWindPlugin.so\">\n");
        sb.append("      <horizontal>\n");
        sb.append("        <magnitude>\n");
        sb.append("          <time_for_rise>10</time_for_rise>\n");
        sb.append("          <sin>\n");
        sb.append("            <amplitude_percent>0.05</amplitude_percent>\n");
        sb.append("            <period>60</period>\n");
        sb.append("          </sin>\n");
        sb.append("          <noise type=\"gaussian\">\n");
        sb.append("            <mean>0</mean>\n");
        sb.append("            <stddev>0.0002</stddev>\n");
        sb.append("          </noise>\n");
        sb.append("        </magnitude>\n");
        sb.append("        <direction>\n");
        sb.append("          <time_for_rise>30</time_for_rise>\n");
        sb.append("          <sin>\n");
        sb.append("            <amplitude>5</amplitude>\n");
        sb.append("            <period>20</period>\n");
        sb.append("          </sin>\n");
        sb.append("          <noise type=\"gaussian\">\n");
        sb.append("            <mean>0</mean>\n");
        sb.append("            <stddev>0.03</stddev>\n");
        sb.append("          </noise>\n");
        sb.append("        </direction>\n");
        sb.append("      </horizontal>\n");
        sb.append("      <vertical>\n");
        sb.append("        <noise type=\"gaussian\">\n");
        sb.append("          <mean>0</mean>\n");
        sb.append("          <stddev>0.03</stddev>\n");
        sb.append("        </noise>\n");
        sb.append("      </vertical>\n");
        sb.append("    </plugin>\n");
        return sb.toString();
    }

    public String buildConclusion(){
        StringBuilder sb = new StringBuilder();
        sb.append("  </world>\n");
        sb.append("</sdf>\n");
        return sb.toString();
    }
}
