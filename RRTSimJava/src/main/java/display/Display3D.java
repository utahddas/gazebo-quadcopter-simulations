package display;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.jzy3d.chart.Chart;
import org.jzy3d.chart.ChartLauncher;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.LineStrip;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.primitives.axes.layout.IAxeLayout;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import state.StateParam;


/*
 display.Display3D.java
 Author: Taylor Welker
 June 4, 2018

 The purpose of this class is to display a 3D plot representing the path of the quadcopter throughout the simulation.

 This class stores the quadcopter's path as waypoints at each timestep. Then, at the end of the simulation, it uses the
 JFreeChart Library to display the quadcopter's path from its initial state (green dot) to its final state (magenta dot).

 An interactive 3D model and a png of the chart will be displayed if the simulation opts to call the "plot_path" method.
 The location of the png file is hard coded into the "plot_path" method, and can be modified there.
*/
public class Display3D {

    private ArrayList<Coord3d> waypoints;
    private Coord3d goalPoint;

    public Display3D(StateParam finalState) {
        waypoints = new ArrayList<>();
        goalPoint = new Coord3d(finalState.getLon(), finalState.getLat(), finalState.getAlt());
    }

    // This function should be called every timestep.  Add the current position to the list.
    public void add_to_waypoints(StateParam initState) {
        waypoints.add(new Coord3d(initState.getLon(), initState.getLat(), initState.getAlt()));
    }

    // This is the function that creates the corresponding 3D plot and png file.
    public void plot_path(StateParam finalState) {

        // Create the arrays that will be passed directly into the plotting object
        Coord3d[] points = new Coord3d[waypoints.size()+1]; // These hold the coordinates
        Color[] colors = new Color[waypoints.size()+1];     // These hold the colors in parallel with the points
        ArrayList<LineStrip> lineStrips = new ArrayList<>();// This is used to draw lines between points

        // Set up a special colored point for the starting position
        points[0] = waypoints.get(0);
        colors[0] = Color.GREEN;

        // For each of the waypoints in our list, add a black point to the graph, and connect it with a line
        for(int i=1; i<waypoints.size(); i++){
            points[i] = waypoints.get(i);               // Get the coordinates
            colors[i] = Color.BLACK;                    // Color it black
            LineStrip ls = new LineStrip();             // Create the connecting line objects
            ls.add(new Point(points[i-1], Color.RED));  // Color the line red, and connect it to the previous dot
            ls.add(new Point(points[i], Color.RED));    // Connect the red line to the current point
            ls.setDisplayed(true);                      // Set it to "displayable"
            lineStrips.add(ls);                         // Add the line to the line creating object
        }

        // Set up a special colored point for the final position
        points[waypoints.size()] = new Coord3d(finalState.getLon(), finalState.getLat(), finalState.getAlt());
        colors[waypoints.size()] = Color.MAGENTA;

        // Create the scatter plot object
        Scatter scatter = new Scatter(points, colors);
        scatter.setWidth(3f);

        // Create a chart object...this is the thing that shows the scatter objects
        Chart chart = AWTChartComponentFactory.chart(Quality.Advanced, "newt");

        // Set up several variables corresponding to the axis labels
        IAxeLayout l = chart.getAxeLayout();
        l.setXAxeLabel("Longitude");
        l.setYAxeLabel("Latitude");
        l.setZAxeLabel("Altitude (m)");
        l.setMainColor(Color.GRAY);
        chart.getAxeLayout().setGridColor(Color.BLUE);

        // Add the scatter plot to the chart
        chart.getScene().add(scatter);

        // Add the lines to the scatter plot chart
        Iterator<LineStrip> IsIterator = lineStrips.iterator();
        while (IsIterator.hasNext()){
            chart.getScene().getGraph().add(IsIterator.next());
        }

        // Open the chart for the user
        ChartLauncher.openChart(chart);

        // Save the image to the default output file
        File image = new File("/home/taylorwelker/RRTSimJava/image.PNG");
        try {
            image.createNewFile();
            chart.screenshot(image);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Getters and Setters
    public ArrayList<Coord3d> getWaypoints() {
        return waypoints;
    }
}
