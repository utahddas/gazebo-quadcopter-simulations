package display;

import javafx.geometry.Point2D;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import state.StateParam;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.util.ArrayList;

/*
 display.Display.java
 Author: Taylor Welker
 June 4, 2018

 The purpose of this class is to display a 2D plot representing the path of the quadcopter throughout the simulation.

 This class is currently deprecated as the 3D model provides a better, more intuitive representation of the data. To fix
 this class description, the naming convention for naming new files, and plots needs to be updated, and the arguments
 to its methods ought to match those of the display.Display3D class.
*/
public class Display {

    private ArrayList<Point2D> waypoints;
    private Point2D goalPoint;
    private String graph_type;

    public Display(StateParam initState, StateParam finalState) {
        if((initState.getLon() == finalState.getLon()) && (initState.getLat() == finalState.getLat())){graph_type = "vrtl";}
        else{graph_type = "hztl";}

        waypoints = new ArrayList<Point2D>();

        if(graph_type.equals("hztl")){
            goalPoint = new Point2D(finalState.getLon(), finalState.getLat());
        }
        else{
            goalPoint = new Point2D(finalState.getLon(), finalState.getAlt());
        }
    }

    public void add_to_waypoints(StateParam initState){
        if(graph_type.equals("hztl")){
            waypoints.add(new Point2D(initState.getLon(), initState.getLat()));
        }
        else{
            waypoints.add(new Point2D(initState.getLon(), initState.getAlt()));
        }
    }

    public void plot_path(StateParam initState, StateParam finalState, double time, double step_size, String miss_num, String wind){

        JFreeChart xylineChart = null;

        if(graph_type.equals("vrtl")) {
            xylineChart = ChartFactory.createXYLineChart(
                    Double.toString(time) + " sec" + " / Wind: " + wind,
                    "Longitude",
                    "Altitude",
                    createDataset(),
                    PlotOrientation.VERTICAL,
                    true, true, false);
        }

        else{
            xylineChart = ChartFactory.createXYLineChart(
                    Double.toString(time) + " sec" + " / Wind: " + wind,
                    "Longitude",
                    "Latitude",
                    createDataset(),
                    PlotOrientation.VERTICAL,
                    true, true, false);
        }

        ChartPanel chartPanel = new ChartPanel(xylineChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 560));
        final XYPlot plot = xylineChart.getXYPlot();

        Shape shape1 = new Ellipse2D.Double(-1.0, -1.0, 2.0, 2.0);
        Shape shape2 = new Ellipse2D.Double(-10.0, -10.0, 20.0, 20.0);
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.RED);
        renderer.setSeriesPaint(1, Color.GREEN);
        renderer.setSeriesShape(0, shape1);
        renderer.setSeriesShape(1, shape2);
        plot.setRenderer(renderer);




        NumberAxis domain = (NumberAxis) plot.getDomainAxis();
        domain.setAutoRangeIncludesZero(false);
        //domain.setRange((Math.min(initState.getLon(), finalState.getLon()) - offset), (Math.max(initState.getLon(), finalState.getLon()) + offset));
        //domain.setTickUnit(new NumberTickUnit(0.001));
        domain.setVerticalTickLabels(true);

        if(graph_type.equals("hztl")) {
            double offset = .0005;
            NumberAxis range = (NumberAxis) plot.getRangeAxis();
            range.setAutoRangeIncludesZero(false);
            //range.setRange((Math.min(initState.getLat(), finalState.getLat()) - offset), (Math.max(initState.getLat(), finalState.getLat()) + offset));
            //range.setTickUnit(new NumberTickUnit(0.001));
            range.setVerticalTickLabels(false);
        }
        else{
            double offset = 5.0;
            NumberAxis range = (NumberAxis) plot.getRangeAxis();
            double min_alt = Math.min(waypoints.get(0).getY(), waypoints.get(waypoints.size() - 1).getY());
            range.setRange((Math.min(min_alt, finalState.getAlt()) - offset), (Math.max(initState.getAlt(), finalState.getAlt()) + offset));
            range.setTickUnit(new NumberTickUnit(1.0));
            range.setVerticalTickLabels(true);
        }


        int width = 640;
        int height = 480;

        try {
            String id = initState.getQuadParam().getQuad_id();
            String model = initState.getQuadParam().getModel();
            String lat = Double.toString(initState.getLat());
            String lon = Double.toString(initState.getLon());
            String alt = Double.toString(initState.getAlt());
            String final_time = Double.toString(initState.getTime());
            String bat = Double.toString(initState.getBattery());
            String filename;
            if(miss_num.equals("none")){
                filename = "src/main/resources/"+id+"_"+model+"_"+lat+"_"+lon+"_"+alt+"_"+final_time+"_"+bat+"_"+step_size+".jpeg";
            }
            else{
                filename = "src/main/resources/preset_missions/mission"+miss_num+"_output/"+wind.toString() + ".jpeg";
            }
            File XYChart = new File(filename);
            XYChart.createNewFile();
            ChartUtils.saveChartAsJPEG(XYChart, xylineChart, width, height);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private XYDataset createDataset() {
        final XYSeries init = new XYSeries("InitialPos");
        for(int i = 0; i < waypoints.size(); i++){
            init.add(waypoints.get(i).getX(), waypoints.get(i).getY());
        }

        final XYSeries goal = new XYSeries("FinalPos");
        goal.add(goalPoint.getX(), goalPoint.getY());

        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(init);
        dataset.addSeries(goal);
        return dataset;
    }

    // Getters
    public ArrayList<Point2D> getWaypoints() {
        return waypoints;
    }
}
