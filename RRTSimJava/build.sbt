name := "RRTSimJava"

version := "0.1"

scalaVersion := "2.12.6"

// https://mvnrepository.com/artifact/org.json/json
libraryDependencies += "org.json" % "json" % "20180130"

// https://mvnrepository.com/artifact/org.jfree/jfreechart
libraryDependencies += "org.jfree" % "jfreechart" % "1.5.0"

// https://groups.google.com/forum/#!searchin/jzy3d/sbt%7Csort:date/jzy3d/4QoqD-CdQZ8/ifWDmOuwAwAJ
// maven.jzy3d.org/releases/org/jzy3d/jzy3d-api/1.0.0
resolvers += "Jzy3d Maven Release Repository" at "http://maven.jzy3d.org/releases/"
libraryDependencies += "org.jzy3d" % "jzy3d-api" % "1.0.0"
//addZipJar("org.jzy3d" % "jzy3d-deps" % "0.9" from "https://www.jzy3d.org/release/0.9a3/org.jzy3d-0.9-dependencies.zip", Compile)

//{
//  val arch = "linux-amd64"
//  addZipJar("org.jzy3d" % "jzy3d-naitive" % "0.9" from "http://www.jzy3d.org/release/0.9a3/org.jzy3d-0.9-binaries-%s.zip".format(arch), Compile)
//}