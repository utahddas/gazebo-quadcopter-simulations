{
	"battery": 1200.0,
	"flight_mode": "fly",
	"lon": -111.844637,
	"time": 0.0,
	"lat": 40.767660,
	"quad_param": {
		"max_hztl_spd": 17.8816,
		"quad_weight": 1.282,
		"battery_lim": 1200.0,
		"quad_id": "iris1",
		"max_vrtl_spd": 5.0,
		"model": "iris+"
	},
	"alt": 10.0,
	"velocity": {
		"x": 0.0,
		"y": 0.0,
		"z": 0.0
	},
	"env_param": [{
		"state.AreaBound": {
			"min_lon": -180.0,
			"max_lon": 180.0,
			"min_lat": -90.0,
			"max_lat": 90.0,
			"min_alt": 5.0,
			"max_alt": 100.0,
			"wind": {
				"x": 0.0,
				"y": 0.0,
				"z": 5.0
			}
		}
	}]
}
